extends AnimatedSprite
var drag_enabled = false
var drag_value = 0
var mouse_pos = 0
var mouse_current = 0
var current_frame = 0

onready var number_frames = get_sprite_frames().get_frame_count("default");

func _process(delta):
	if drag_enabled:
		mouse_current = get_global_mouse_position();
		drag_value = mouse_pos.x - mouse_current.x;
		if drag_value > 0:
			current_frame += 1;
		if drag_value < 0:
			current_frame -= 1;
			
		if current_frame > number_frames:
			current_frame = 0;
		elif current_frame < 0:
			current_frame = number_frames;
		
		set_frame(current_frame);
		mouse_pos = get_global_mouse_position();
		
func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				drag_enabled = true;
				mouse_pos = get_global_mouse_position();
			else:
				drag_enabled = false;
